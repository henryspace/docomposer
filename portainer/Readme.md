### 什么是Portainer

Portainer是Docker的图形化管理工具，提供状态显示面板、应用模板快速部署、容器镜像网络数据卷的基本操作（包括上传下载镜像，创建容器等操作）、事件日志显示、容器控制台操作、Swarm集群和服务等集中管理和操作、登录用户管理和控制等功能。功能十分全面，基本能满足中小型单位对容器管理的全部需求。

### 安装docker
```

# Ubuntu
apt-get -y update
apt-get -y install docker.io
systemctl start docker && systemctl enable docker
 
# CentOS 6
rpm -iUvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
yum update -y
yum -y install docker-io
service docker start
chkconfig docker on

# CentOS 7、Debian、Ubuntu
curl -sSL https://get.docker.com/ | sh
systemctl start docker
systemctl enable docker.service

```

### Portainer中文汉化
下载汉化文件
https://www.quchao.net/Portainer-CN.html
百度云（提取码: nzue）
https://pan.baidu.com/s/13ra6jXHR_7vajLLlf5GVEw

新建文件夹命名为 public ，把 Portainer-CN.zip 解压至里面

```
docker volume create portainer_data
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data -v /public:/public portainer/portainer:1.20.2
```

您只需要使用浏览器访问运行Portainer的Docker引擎的端口http://127.0.0.1:9000















