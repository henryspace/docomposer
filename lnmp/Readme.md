### LNMP
docker for building lnmp

date: 2021-05-11 16:05:44

### 功能说明
> 本项目通过docker-compose配置编排lnmp环境，集成了PHP(7.4)、Nginx(最新版)、Mysql(5.7)、Redis(最新版)等，通过docker-compose命令启动后可直接运行PHP项目。


### 目录说明

```
# docker-compose配置映射docker目录结构

lnmp
├── data
├────── mysql Mysql数据在主机存放位置 
├────── redis Redis数据在主机存储位置
├── logs
├────── nginx Nginx日志在主机存储位置
├────── php   PHP日志在主机存储位置
├── services
├────── mysql Mysql配置文件在主机存放位置
├────── nginx Nginx配置文件在主机存储位置
├────── php   PHP配置文件在主机存储位置 
├────── redis Redis配置文件在主机存储位置
├── sites
├────── frontend 前端项目
├────── backend	 后端项目
├────── project  其他项目
├── docker-compose.yml 配置文件 YAML声明式文件
├── Readme.md    # 项目说明文件       
└──
```

### 安装Docker

Ubuntu:
```
# 更新系统安装包
sudo apt-get -y update

# 安装docker
sudo apt-get -y install docker.io

# 启动docker并设置开机自启
sudo systemctl start docker && systemctl enable docker

```

Centos:
```
# 更新系统安装包
sudo yum -y update

# 安装docker
sudo curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun

# 启动docker并设置开机自启
sudo systemctl start docker && systemctl enable docker

```

Windows/Mac:
```
# 下载安装Docker官方客户端

下载地址：https://www.docker.com/products/docker-desktop/

```

### 安装Docker-compose

> Docker Compose是 docker 提供的一个命令行工具，用来定义和运行由多个容器组成的应用。使用 compose，我们可以通过 YAML 文件声明式的定义应用程序的各个服务，并由单个命令完成应用的创建和启动。（Windows/Mac客户端自带，不用安装，此步骤可省略）

```
# 下载安装
sudo curl -L https://get.daocloud.io/docker/compose/releases/download/2.3.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose

# 配置权限
sudo chmod +x /usr/local/bin/docker-compose

# 配置全局可用
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# 查看安装版本
sudo docker-compose --version

```

### 启动lnmp环境
```
# 控制台执行

cd Git目录

git clone https://gitee.com/henryspace/docomposer.git

cd docomposer/lnmp

sudo docker-compose up -d


```



